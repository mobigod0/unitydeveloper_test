﻿using UnityEngine;
using System.Collections;

public class SwordTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if((transform.parent.CompareTag("Player") && col.CompareTag("Enemy")) ||
            (transform.parent.CompareTag("Enemy") && col.CompareTag("Player")))
            transform.parent.SendMessage("AddEnemyToList", col.gameObject);
    }

    void OnTriggerExit(Collider col)
    {
        if ((transform.parent.CompareTag("Player") && col.CompareTag("Enemy")) ||
            (transform.parent.CompareTag("Enemy") && col.CompareTag("Player")))
            transform.parent.SendMessage("RemoveEnemyFromList", col.gameObject);
    }
}
