﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    GameObject mainScreen, infoScreen;

    void Start()
    {
        mainScreen = GameObject.Find("MainScreen");
        infoScreen = GameObject.Find("InfoScreen");
        infoScreen.SetActive(false);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void OpenInfo()
    {
        mainScreen.SetActive(false);
        infoScreen.SetActive(true);
    }

    public void CloseInfo()
    {
        mainScreen.SetActive(true);
        infoScreen.SetActive(false);
    }
}
