﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class PlayerController : MonoBehaviour
{
    public float movingSpeed = 7; // скорость перемещения
    public float rotatingSpeed = 1; // скорость поворота вокруг своей оси
    public GameObject sword;
    public GameObject shield;
    public List<GameObject> enemyInColliderList = new List<GameObject>(); // список врагов, которых мы можем уничтожить в данный момент
    public AnimationClip hitAnim; // анимация удара мечом

    private AudioClip breakShield, swordHit, swordSwing; // различные звуки
    private CharacterController charController; 
    private Vector3 moveDirection = Vector3.zero; // куда будет перемещаться герой 
    private bool attacked; // нанесли уже урон врагу в текущий удар мечом?
    private bool animPlaying; // играет ли анимация удара мечом?
    private float hitAnimLen; // длина анимации удара мечом
    private float hitTime; // время удара мечом

    void Start()
    {
        charController = GetComponent<CharacterController>();
        hitAnimLen = hitAnim.length;

        // загружаем звуки из AssetBundle
        var audioAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "audio"));
        if (audioAssetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle!");
            return;
        }
        
        swordHit = audioAssetBundle.LoadAsset<AudioClip>("SwordAttack");
        breakShield = audioAssetBundle.LoadAsset<AudioClip>("BreakShield");
        swordSwing = audioAssetBundle.LoadAsset<AudioClip>("SwordSwing");
        audioAssetBundle.Unload(false);
    }

    // Чтобы не было подёргиваний при перемещении персонажа,
    // обработку перемещения выполняем здесь
    void FixedUpdate()
    {
        moveDirection = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            moveDirection += transform.forward * movingSpeed;
        }

        if (Input.GetKey(KeyCode.S))
        {
            moveDirection -= transform.forward * movingSpeed;
        }

        if (Input.GetKey(KeyCode.Space) && charController.isGrounded)
        {
            moveDirection.y = 50;
        }
        moveDirection.y -= 50 * Time.deltaTime; // гравитация
        charController.Move(moveDirection * Time.deltaTime);
    }

	void Update ()
	{
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(transform.up, -rotatingSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(transform.up, rotatingSpeed);
        }

        // hitAnimLen делим пополам, т.к. анимация ускорена в 2 раза
        if (Time.time - hitTime >= hitAnimLen/2 && animPlaying)
	        animPlaying = false;

        if (Input.GetMouseButton(0) && !animPlaying) // если нажата левая кнопка мыши и анимация не играет
        {
            GetComponent<AudioSource>().PlayOneShot(swordSwing); // звук рассекания мечом воздуха
            hitTime = Time.time; // запоминаем время удара
            animPlaying = true;
            sword.GetComponent<Animator>().SetTrigger("hitTrigger"); // выставляем триггер для анимации
            attacked = false;
        }
        if (animPlaying && !attacked)
            TryToAttack(); // атакуем

        // если нажали правую кн. мыши и у нас ещё есть щит
	    if (Input.GetMouseButtonDown(1) && GetComponent<Health>().shieldHealth > 0)
	    {
            shield.GetComponent<Animator>().SetTrigger("def");
	        GetComponent<Health>().shielded = true;
	    }
	    if (Input.GetMouseButtonUp(1) && GetComponent<Health>().shieldHealth > 0)
	    {
            shield.GetComponent<Animator>().SetTrigger("undef");
            GetComponent<Health>().shielded = false;
        }
    }

    // функция ломания щита
    public void BreakShield()
    {
        Destroy(shield);
        GetComponent<AudioSource>().PlayOneShot(breakShield);
    }

    void TryToAttack()
    {
        if (enemyInColliderList.Count > 0)
        {
            // список копируется для того, чтобы избежать ошибок при его изменении (если противник умирает)
            var copyList = new List<GameObject>(enemyInColliderList);
            foreach (var enemy in copyList)
            {
                GetComponent<AudioSource>().PlayOneShot(swordHit);
                attacked = true;
                enemy.GetComponent<Health>().GetHit();
                if (enemy.GetComponent<Health>().healthCount <= 0)
                    enemyInColliderList.Remove(enemy);
            }
        }
    }

    // функция вызывается при попадании мечом по противнику
    void AddEnemyToList(GameObject enemy)
    {
        if (!enemyInColliderList.Exists(en => en == enemy))
            enemyInColliderList.Add(enemy);
    }

    void RemoveEnemyFromList(GameObject enemy)
    {
        enemyInColliderList.Remove(enemy);
    }
}
