﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private List<GameObject> hearts = new List<GameObject>(), shields = new List<GameObject>();
    private GameObject UI;

    void Start()
    {
        Time.timeScale = 1f;

        UI = GameObject.Find("UI");

        var imagesAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "images"));
        if (imagesAssetBundle == null)
        {
            Debug.LogError("Failed to load AssetBundle!");
            return;
        }

        var player = GameObject.FindGameObjectWithTag("Player");
        var heart = imagesAssetBundle.LoadAsset<Sprite>("heart");
        var shield = imagesAssetBundle.LoadAsset<Sprite>("Shield");
        imagesAssetBundle.Unload(false);
        
        // создаём сердечки для интерфейса
        for (int i = 0; i < player.GetComponent<Health>().healthCount; i++)
        {
            var heartImage = new GameObject("Heart");
            heartImage.AddComponent<Image>();
            heartImage.GetComponent<Image>().sprite = heart;
            heartImage.transform.SetParent(UI.transform.FindChild("LifeAndShield/PlaceForHearts"));
            hearts.Add(heartImage);
        }

        // создаём значки щита для интерфейса
        for (int i = 0; i < player.GetComponent<Health>().shieldHealth; i++)
        {
            var shieldImage = new GameObject("Shield");
            shieldImage.AddComponent<Image>();
            shieldImage.GetComponent<Image>().sprite = shield;
            shieldImage.transform.SetParent(UI.transform.FindChild("LifeAndShield/PlaceForShields"));
            shields.Add(shieldImage);
        }


    }

    // функция вызывается при убийстве героем всех противников на уровне
    public void PlayerWin(float time, int killedEnemies)
    {
        WriteInfo(time, killedEnemies, true);
    }

    // функция вызывается при гибели героя
    public void PlayerLose(float time, int killedEnemies)
    {
        WriteInfo(time, killedEnemies, false);
    }

    // Функция вывода информации о результате игры на экран
    void WriteInfo(float time, int killedEnemies, bool win)
    {
        Time.timeScale = 0.2f;

        var panel = UI.transform.FindChild("Panel").gameObject;
        panel.SetActive(true);

        var textField = panel.transform.FindChild("WinOrLose").GetComponent<Text>();
        textField.text = win ? "Поздравляю, Вы спасли мир от нашествия серых кубов!" : "К сожалению, Вы не смогли спасти мир от серых кубов, но всегда можно попробовать ещё раз!";

        textField = panel.transform.FindChild("Time").GetComponent<Text>();
        textField.text = "Время, затраченное на прохождение: ";

        var allTime = TimeSpan.FromSeconds(time); // переводим время из секунд в минуты/секунды

        if (allTime.Minutes > 0)
        {
            textField.text += allTime.Minutes + " минут";
            if (allTime.Seconds > 0)
                textField.text += ", " + allTime.Seconds + " секунд";
        } else
        if (allTime.Seconds > 0)
            textField.text += allTime.Seconds + " секунд";

        textField = panel.transform.FindChild("Enemies").GetComponent<Text>();
        textField.text = "Убито врагов: " + killedEnemies;
    }

    // функция вызывается сообщением при получении урона героем
    void PlayerHitted(bool shielded)
    {
        if (!shielded)
        {
            Destroy(hearts[0]);
            hearts.RemoveAt(0);
        }
        else
        {
            Destroy(shields[0]);
            shields.RemoveAt(0);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene("Level1");
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
