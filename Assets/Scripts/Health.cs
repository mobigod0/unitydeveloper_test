﻿using UnityEngine;
using System.Collections;
using System.IO;

[RequireComponent(typeof(AudioSource))]
public class Health : MonoBehaviour
{
    public int healthCount = 1; // количество очков жизни
    public bool shielded; // защищен ли щитом
    public int shieldHealth = 4; // прочность щита

    private GameObject UIController;
    private GameObject gameManager;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        UIController = GameObject.Find("UIController");
    }

    public void GetHit()
    {
        if (CompareTag("Player"))
            UIController.SendMessage("PlayerHitted", shielded);
        if (shielded && shieldHealth > 0)
        {
            shieldHealth--;
            if (shieldHealth <= 0)
            {
                shielded = false;
                gameObject.SendMessage("BreakShield");
            }
            return;
        }
        healthCount--;
        if (healthCount <= 0)
            Die();
    }

    void Die()
    {
        gameManager.SendMessage("AnotherDeath", gameObject);
        Destroy(gameObject);
    }
}
