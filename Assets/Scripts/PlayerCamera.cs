﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    GameObject hero; // gameObject героя
    bool rotatingCam; // поворачиваем ли мы камеру
    bool cameraMoved; // переместилась ли камера из-за препятствия
    Vector3 savedPos; // сохранённая позиция камеры

    void Start()
    {
        hero = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(RotateRPGCam());
        savedPos = transform.position = hero.transform.FindChild("CameraCenter").position;
    }

	void Update () {
        if(!hero)
            Destroy(this);

        if (Input.GetKey(KeyCode.Q))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("CameraLeft").rotation, 0.1f); // поворачиваем камеру влево
            rotatingCam = true;
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("CameraRight").rotation, 0.1f); // поворачиваем камеру вправо
            rotatingCam = true;
        }

        if (Input.GetKeyUp(KeyCode.E) || Input.GetKeyUp(KeyCode.Q))
            rotatingCam = false;

        // если камера не находится за игроком
        if (savedPos != hero.transform.FindChild("CameraCenter").position)
        {
            // перемещаем её туда
            transform.position = savedPos = hero.transform.FindChild("CameraCenter").position;
            // камеру ещё не перемещали
            cameraMoved = false;
        }

        RaycastHit hit;
        Ray ray = new Ray(transform.position, hero.transform.position - transform.position); // пускаем луч из камеры в сторону игрока
        while (Physics.Raycast(ray, out hit, 100f) && !hit.collider.CompareTag("Player") && !hit.collider.CompareTag("Enemy")) // пока есть препятствие между камерой и игроком
        {
            transform.position = Vector3.Lerp(transform.position, hero.transform.position, 0.1f); // приближаем камеру
            if (Vector3.Distance(transform.position, hero.transform.position) <= 0.3f)
                break;
            ray = new Ray(transform.position, hero.transform.position - transform.position); // пускаем новый луч
            cameraMoved = true; // камеру переместили
        }

        // если камеру не перемещали 
        if (!cameraMoved)
        {
            // пока нет препятствий между камерой и центральным положением камеры за игроком 
            while (!Physics.Raycast(transform.position, savedPos - transform.position, 0.3f) && Vector3.Distance(transform.position, savedPos) > 0.1f)
            {
                // возвращаем камеру 
                transform.position = Vector3.Lerp(transform.position, savedPos, 0.1f);
            }
        }
    }

    IEnumerator RotateRPGCam()
    {
        while (true)
        {
            if (!rotatingCam) // если не нажата Q или E
                transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("CameraCenter").rotation, 0.1f); // возвращаем камеру к центру
            yield return new WaitForEndOfFrame();
        }
    }
}
