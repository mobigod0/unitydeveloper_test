﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    private int enemiesCount = 0, enemiesCountAtStart = 0;

    void Start()
    {
        enemiesCountAtStart = enemiesCount = GameObject.FindGameObjectsWithTag("Enemy").Length; // считаем количество врагов 
    }

    // функция вызывается сообщением, когда кто-либо умирает (игрок или враг)
    public void AnotherDeath(GameObject who)
    {
        if (who.CompareTag("Enemy"))
        {
            enemiesCount--;
            if (enemiesCount <= 0)
                GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().PlayerWin(Time.time, enemiesCountAtStart);
        }

        if (who.CompareTag("Player"))
            GameObject.FindGameObjectWithTag("UIController").GetComponent<UIController>().PlayerLose(Time.time, enemiesCountAtStart - enemiesCount);
    }
}
