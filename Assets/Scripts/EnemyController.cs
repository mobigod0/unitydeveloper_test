﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class EnemyController : MonoBehaviour
{
    public AnimationClip hitAnim; // анимация удара

    private NavMeshAgent agent; 
    private bool patroling = true; // ищет ли игрока данный противник
    private GameObject player;
    private List<GameObject> enemyInColliderList = new List<GameObject>(); // список врагов, которых мы можем атаковать
    private GameObject sword; 
    private bool attacked; // атаковали ли мы уже в эту анимацию
    private bool animPlaying; // играет ли ещё анимация удара
    private float hitAnimLen; // длительность анимации удара
    private float hitTime; // время, когда ударили мечом
    private AudioClip swordHit, shieldHit, swordSwing;

    void Start()
    {
        sword = transform.FindChild("Sword").gameObject;
        agent = GetComponent<NavMeshAgent>();
        GoToRandomPoint(); // запускаем хаотичное перемещение

        hitAnimLen = hitAnim.length;

        // загружаем звуки из AssetBundle
        var audioAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "audio"));
        if (audioAssetBundle == null)
        {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }

        swordHit = audioAssetBundle.LoadAsset<AudioClip>("SwordAttack");
        swordSwing = audioAssetBundle.LoadAsset<AudioClip>("SwordSwing");
        shieldHit = audioAssetBundle.LoadAsset<AudioClip>("ShieldHit");
        audioAssetBundle.Unload(false);
    }

    void GoToRandomPoint()
    {
        agent.SetDestination(new Vector3(Random.Range(-198, 198), 0, Random.Range(-198, 198))); // отправляемся в случайную точку на карте
    }

    // вызывается при столкновении коллайдера с игроком
    void FindPlayer(GameObject player) 
    {
        this.player = player.transform.FindChild("PlayerFace").gameObject;
        GetComponent<AudioSource>().Play();
        GetComponent<MeshRenderer>().material.color = Color.red;
        patroling = false;
        agent.SetDestination(this.player.transform.position);
    }

    void Update()
    {
        if (patroling && agent.remainingDistance <= 0.5f) // если игрок ещё не найден и мы пришли к крайней точке
            GoToRandomPoint(); // идём куда-нибудь ещё
        if (!patroling && player)
        {
            agent.SetDestination(player.transform.position); // идём к игроку
            transform.LookAt(player.transform); // смотрим на игрока

            // hitAnimLen делим пополам, т.к. анимация ускорена в 2 раза
            if (Time.time - hitTime >= hitAnimLen / 2 && animPlaying)
                animPlaying = false;

            // если мы находимся на дистанции удара и не бьём мечом
            if (Vector3.Distance(transform.position, player.transform.position) <= 1f + agent.stoppingDistance && !animPlaying)
            {
                GetComponent<AudioSource>().PlayOneShot(swordSwing);
                hitTime = Time.time;
                animPlaying = true;
                attacked = false;
                sword.GetComponent<Animator>().SetTrigger("hitTrigger"); // выставляем триггер для анимации
            }
            if (animPlaying && !attacked)
                TryToAttack(); // атакуем
        }
    }

    void TryToAttack()
    {
        var copyList = new List<GameObject>(enemyInColliderList);
        foreach (var enemy in copyList)
        {
            if(enemy.GetComponent<Health>().shielded)
                GetComponent<AudioSource>().PlayOneShot(shieldHit);
            else
                GetComponent<AudioSource>().PlayOneShot(swordHit);
            attacked = true;
            enemy.GetComponent<Health>().GetHit();
            if (enemy.GetComponent<Health>().healthCount <= 0)
            {
                RemoveEnemyFromList(enemy);
                GoToRandomPoint();
                GetComponent<MeshRenderer>().material.color = new Color(0.23f, 0.23f, 0.23f);
                patroling = true;
            }
        }
    }

    void AddEnemyToList(GameObject enemy)
    {
        if(!enemyInColliderList.Exists(en => en == enemy))
            enemyInColliderList.Add(enemy);
    }

    void RemoveEnemyFromList(GameObject enemy)
    {
        enemyInColliderList.Remove(enemy);
    }
}
