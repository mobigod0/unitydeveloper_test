﻿using UnityEngine;
using System.Collections;

public class EnemyDetector : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            transform.parent.SendMessage("FindPlayer", col.gameObject);
            Destroy(gameObject);
        }
    }
}
